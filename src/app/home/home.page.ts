import { Component } from '@angular/core';
import { DecimalPipe } from "@angular/common";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  // Member variables for input and output data.
weight: number;
bottles: number;
time: number;
promille: number;
gender: string;

  constructor() {
    // Set the initial values for member variables.
    // These are displayed when the app is opened.
    this.weight = 0;
    this.bottles = 0;
    this.time = 0;
    this.promille = 0;
  }

  calculate() {
    var litre = this.bottles * 0.33;
    var gram = litre * 8 * 4.5

    var burn = this.weight / 10
    
    var gramleft = gram - (burn * this.time)

    if(this.gender == "male"){
      this.promille = gram / (this.weight * 0.7);
    } else if (this.gender == "female"){
      this.promille = gram / (this.weight * 0.6);
    }
  }

}
